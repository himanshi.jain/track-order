import React, { Component } from "react";
import { Chrono } from "react-chrono";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Moment from 'moment';
import axios from 'axios';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
    this.state = {
      trackOrderData: [],
      rowMarkup: [],
      flag: 0,
      orderId: [],
      waybillArr: [],
      popUpActive: false
    }
  }

  componentDidMount = async () => {
    // let wayBillNo=req.query.waybill_number;
    // let orderId=req.query.orderId;
    let waybillNo = '5443210001352,5443210001481';
    let orderId = 1058;
    if (waybillNo) {
      // let response = await axios.get(`http://localhost:5000/tracking/orders?shop=symphony-limited.myshopify.com&waybill_number=${waybillNo}`)
      // if (response.data.ShipmentData.length > 0) {
      //     this.getCardData(response.data.ShipmentData, waybillNo.split(','));
      //     this.setState({ flag: 0 });
      // } else {
      //     this.setState({ popUpActive: true })
      // }
    }
    if (orderId) {
      let response = await axios.get(`http://localhost:5000/tracking/orders?shop=symphony-limited.myshopify.com&orderId=${orderId}`);
      if (response.data.status) {
        this.getCardData(response.data.ShipmentData.ShipmentData, response.data.WayBill);
        this.setState({ flag: 1, orderId: response.data.OrderName, waybillArr: response.data.WayBill });
      } else {
        this.setState({ popUpActive: true })
      }
    }
  }

  setTrackingIndex = (shipment) => {
    let itemsArr = [{
      cardTitle: <p ><i className="fa-solid fa-boxes-packing"></i>  Pick Up Pending</p>,
      cardDetailedText: [],
    }, {
      cardTitle: <p ><i className="fa-solid fa-truck"></i>  In-transit</p>,
      cardDetailedText: []
    }, {
      cardTitle: <p ><i className="fa-solid fa-motorcycle"></i>  Out for delivery</p>,
      cardDetailedText: []
    }, {
      cardTitle: <p><i className="fa-solid fa-people-carry-box"></i>  Delivered</p>,
      cardDetailedText: []
    }];
    let trackIndexValue = this.state.trackIndexValue;
    let trackingIndex = this.state.trackingIndex;

    let shipmentLength = shipment.length;
    trackIndexValue = shipment[shipmentLength - 1].Status;

    if (trackIndexValue === 'Manifested' || trackIndexValue === 'Not Picked') {
      trackingIndex = 0
    } else if ((trackIndexValue === 'In Transit' || trackIndexValue === 'Pending') && shipment[shipmentLength - 1].StatusType === 'RT') {
      trackingIndex = 2;
      itemsArr[0].cardTitle = <p><i className="fa-solid fa-boxes-packing"></i>  Picked Up</p>
      itemsArr[1].cardTitle = <p><i className="fa-solid fa-truck"></i>  Undelivered</p>
      itemsArr[2].cardTitle = <p><i className="fa-solid fa-motorcycle"></i>  In-transit for return</p>
      itemsArr[3].cardTitle = <p><i className="fa-solid fa-people-carry-box"></i>  Returned</p>
    } else if ((trackIndexValue === 'In Transit' || trackIndexValue === 'Pending') && shipment[shipmentLength - 1].StatusType === 'PU') {
      trackingIndex = 1;
      itemsArr[0].cardTitle = <p><i className="fa-solid fa-boxes-packing"></i>  Picked Up</p>
      itemsArr[1].cardTitle = <p><i className="fa-solid fa-truck"></i>  In-transit for return</p>
      itemsArr[2].cardTitle = <p><i className="fa-solid fa-motorcycle"></i>  Out for return</p>
      itemsArr[3].cardTitle = <p><i className="fa-solid fa-people-carry-box"></i>  Returned</p>
    } else if (trackIndexValue === 'In Transit' || trackIndexValue === 'Pending') {
      trackingIndex = 1;
      itemsArr[0].cardTitle = <p><i className="fa-solid fa-boxes-packing"></i>  Picked Up</p>
    } else if (trackIndexValue === 'Dispatched') {
      trackingIndex = 2;
      itemsArr[0].cardTitle = <p><i className="fa-solid fa-boxes-packing"></i>  Picked Up</p>
    } else if (trackIndexValue === 'Delivered') {
      trackingIndex = 3;
      itemsArr[0].cardTitle = <p><i className="fa-solid fa-boxes-packing"></i>  Picked Up</p>
    } else if (trackIndexValue === 'RTO' || trackIndexValue === 'DTO') {
      trackingIndex = 3;
      itemsArr[0].cardTitle = <p><i className="fa-solid fa-boxes-packing"></i>  Picked Up</p>
    }
    return {
      itemsArr: itemsArr,
      trackIndexValue: trackIndexValue,
      trackingIndex: trackingIndex
    };
  }


  getTableData = (cardData) => {
    var rows = []
    cardData.forEach(function (data, index) {
      rows.push(
        <tr key={index} >
          <td className="table-row-size-style">{data.dateAndTime} </td>
          <td className="table-row-size-style">{data.city} </td>
          <td className="table-row-size-style">{data.status}</td>
        </tr >
      );
    });
    return (
      <div>
        <table>
          <thead>
            <tr>
              <th className="table-heading">Date</th>
              <th className="table-heading">City</th>
              <th className="table-heading" >Status</th>
            </tr>
          </thead>
          <tbody>{rows}</tbody>
        </table>
      </div>
    )
  }

  getCardData = (shipmentData, wayBillNo) => {

    let rowMarkup = [];
    rowMarkup = shipmentData.map((waybillObj, index) => {
      let setTracking = this.setTrackingIndex(waybillObj.Shipment);
      let itemsArr = setTracking.itemsArr
      let trackIndexValue = setTracking.trackIndexValue;
      let trackingIndex = setTracking.trackingIndex;
      let shipment = waybillObj.Shipment;
      let manifestedData = [];
      let inTransitData = [];
      let dispatchData = [];
      let deliveryData = [];

      shipment.map((shipmentObj, index) => {
        if (shipmentObj.Scan === "Manifested" || shipmentObj.Scan === "Open" || shipmentObj.Scan === "Scheduled" || shipment.Scan === "Not Picked") {
          let manifestedObj = {
            dateAndTime: Moment(shipmentObj.StatusDateTime).format('DD-MM-YYYY'),
            city: shipmentObj.ScannedLocation,
            status: shipmentObj.Instructions
          }

          manifestedData.push(manifestedObj);
          if (shipmentObj.Scan === "Open" || shipmentObj.Scan === "Scheduled") {

            itemsArr[3].cardTitle = <p ><i className="fa-solid fa-people-carry-box"></i>  Returned</p>
          }
        } else if (shipmentObj.Scan === "In Transit" || shipmentObj.Scan === "Pending") {
          if (shipmentObj.ScanType === "RT" && shipmentObj.Scan === "In Transit") {

            itemsArr[3].cardTitle = <p ><i className="fa-solid fa-people-carry-box"></i>  Returned</p>
          }
          if (trackIndexValue === 'RTO' && shipmentObj.ScanType === "UD") {
          } else {
            let inTransitObj = {
              dateAndTime: Moment(shipmentObj.StatusDateTime).format('DD-MM-YYYY'),
              city: shipmentObj.ScannedLocation,
              status: shipmentObj.Instructions
            }
            inTransitData.push(inTransitObj)

            if (shipmentObj.ScanType === "RT") {
              itemsArr[1].cardTitle = <p ><i className="fa-solid fa-truck"></i>  Undelivered</p>
            }
            if (shipmentObj.ScanType === "PU") {
              itemsArr[1].cardTitle = <p><i className="fa-solid fa-truck"></i>  In-transit for return</p>
            }
          }
        } else if (shipmentObj.Scan === "Dispatched") {

          if (shipmentObj.Scan === "Dispatched" && shipmentObj.ScanType === "PP") {
            itemsArr[2].cardTitle = <p ><i className="fa-solid fa-motorcycle"></i>  Out for return</p>

            let manifestedObj = {
              dateAndTime: Moment(shipmentObj.StatusDateTime).format('DD-MM-YYYY'),
              city: shipmentObj.ScannedLocation,
              status: shipmentObj.Instructions
            }
            manifestedData.push(manifestedObj);

          } else if (trackIndexValue === 'RTO' && shipmentObj.ScanType === "UD") {
          } else {
            let dispatchObj = {
              dateAndTime: Moment(shipmentObj.StatusDateTime).format('DD-MM-YYYY'),
              city: shipmentObj.ScannedLocation,
              status: shipmentObj.Instructions
            }
            dispatchData.push(dispatchObj);
          }

          if (shipmentObj.ScanType === "RT") {

            itemsArr[2].cardTitle = <p ><i className="fa-solid fa-motorcycle"></i>  In-transit for return</p>
          }

        } else if (shipmentObj.Scan === "Delivered" || shipmentObj.Scan === "RTO" || shipmentObj.Scan === "DTO") {

          let deliveryObj = {
            dateAndTime: Moment(shipmentObj.StatusDateTime).format('DD-MM-YYYY'),
            city: shipmentObj.ScannedLocation,
            status: shipmentObj.Instructions
          }
          deliveryData.push(deliveryObj);

          if (shipmentObj.Scan === "RTO" || shipmentObj.Scan === "DTO") {
            itemsArr[3].cardTitle = <p ><i className="fa-solid fa-people-carry-box"></i>  Returned</p>
          }

        }

      })

      if (manifestedData.length !== 0) {
        console.log("manifestedData", manifestedData)
        var manifestedTableData = this.getTableData(manifestedData);
        itemsArr[0].cardDetailedText = manifestedTableData;
      }
      if (inTransitData.length !== 0) {
        var inTransitTableData = this.getTableData(inTransitData);
        itemsArr[1].cardDetailedText = inTransitTableData;
      }
      if (dispatchData.length !== 0) {
        let dispatchTableData = this.getTableData(dispatchData);
        itemsArr[2].cardDetailedText = dispatchTableData;
      }
      if (deliveryData.length !== 0) {
        let deliveryTableData = this.getTableData(deliveryData);
        itemsArr[3].cardDetailedText = deliveryTableData;
      }

      if (trackIndexValue === 'RTO') {
        itemsArr[0].cardDetailedText = [];
      }

      return (
        <div className="tracking-detail-card-wrapper card-position" key={index} data-cardkey={index} ref={this.myRef}>
          <div className="card">
            <h1 className="title">WayBill Number : {wayBillNo[index]}</h1>
            <Card >
              <CardContent>
                <Chrono
                  items={itemsArr}
                  mode="VERTICAL"
                  allowDynamicUpdate={true}
                  activeItemIndex={trackingIndex}
                  borderLessCards={true}
                  disableClickOnCircle={true}
                  hideControls={true}
                  theme={{
                    primary: "grey",
                    secondary: "green",
                    cardForeColor: "black",
                    cardBgColor: '#f9f9f9'
                  }}
                  useReadMore={true}
                  disableAutoScrollOnClick={true}
                >
                </Chrono >
              </CardContent>
            </Card>
          </div>
        </div>)
    })
    if (rowMarkup.length > 0) {
      this.setState({ rowMarkup: rowMarkup })
    }

  }
  goToCard = (waybillCount) => {
    console.log(waybillCount)
    let wayBillIndex;
    let element = this.state.waybillArr;
    for (let index in element) {
      if (element[index] === waybillCount[0])
        wayBillIndex = index
    }
    let card = document.querySelectorAll(".card-position")
    card.forEach((el) => {
      if (el.dataset.cardkey === wayBillIndex) {
        window.scrollTo({ top: el.offsetTop })
      }
    })
  }

  render() {
    console.log("render")
    return (
      <div>
        {this.state.popUpActive ?
          <div className="not-found-card" >
            <Card className="not-found-card-style">
              <CardContent>

                NO DETAILS FOUND

              </CardContent>
            </Card>
          </div>
          : <div>
            {this.state.flag === 1 ?
              <div className="order-name-track">
                TRACKING DETAILS
                <div className="tracking-order-name-list">
                  {
                    this.state.orderId.map((value) => (
                      <li onClick={() => this.goToCard(value.waybillCount)}><u>{value.orderName}</u> : {value.waybillCount.toString()}</li>
                    ))
                  }
                </div>
              </div> : <div></div>
            }
            <div className="tracking-detail">
              {this.state.rowMarkup}
            </div>
          </div>
        }
      </div>
    )
  }

}
export default App